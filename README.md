# base-laravel

#### 介绍
本人常用的，一些对laravel的封装，即公共方法等

#### 软件架构
软件架构说明


#### 安装教程

1. composer require peer/base-laravel

#### 使用说明

> 修改在 config/app.php 
> * 添加服务 Peer\Base\BaseServiceProvider::class
> * 添加门面 'TResponse' => Peer\Base\Facades\TResponse::class,

#### 具体说明

1. 里面封装了与API资源类相关的响应处理
2. 封装了API资源集合 对数据源的定义处理（与API资源类一致）
3. 封装了统一的数据响应类（门面）

#### API资源类使用（具体参考官方文档）

例如我们现在有个user的orm模型，要生成API资源类
`php artisan make:resource UserResource`
然后将里面的
`use Illuminate\Http\Resources\Json\Resource;`
替换为
`use Peer\Base\Resource;`

#### API资源集合类使用（具体参考官方文档）

例如我们现在有个user的orm模型，要生成API资源集合类
`php artisan make:resource UserCollection`
然后将里面的
`use Illuminate\Http\Resources\Json\ResourceCollection;`
替换为
`use Peer\Base\ResourceCollection;`

其中里面定义了，可以对列表的数据源格式进行修改,只需定义方法
(```)
    public function setCollectionItem($user){
        return array(
            'user_id' => $user->user_id,
            'account' => $user->account,//$this->when($user->isAdmin, $user->account),
            'nickname' => $user->nickname,
            'status' => $user->status,
            'create_time' => $user->create_time,
            'update_time' => $user->update_time,
        );
    }
(```)

#### 数据响应门面用法

* 例子：
   `return TResponse::success()->setData(['info'=>'hello world'])->response();`
* 具体api：
>> 表示状态：
> `setStatus(int $status)`
> `success()`
> `error()`
> `loginInvalid()`
> `noPower()`
> `noAllow()`
> `update()`
>> 设置数据：
> `setData(array $data)`
>> 设置错误:
> `setError(array $errors)`
>> 设置信息：
> `setMsg(string $msg)`