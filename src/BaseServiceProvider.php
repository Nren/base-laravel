<?php

namespace Peer\Base;

use Illuminate\Support\ServiceProvider;
use Peer\Base\TResponse;

class BaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tresponse',function(){
            return new TResponse();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/base.php' => config_path('base.php'), // 发布配置文件到 laravel 的config 下
        ]);
    }
}
