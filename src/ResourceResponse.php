<?php

namespace Peer\Base;

use Illuminate\Http\Resources\Json\ResourceResponse as BaseResourceResponse;
use Peer\Base\Facades\TResponse;

class ResourceResponse extends BaseResourceResponse{
    
    public function toResponse($request){
        return tap(TResponse::success()->setData($this->wrap(
                $this->resource->resolve($request),
                $this->resource->with($request),
                $this->resource->additional
            ))->response(), function ($response) use ($request) {
                $response->original = $this->resource->resource;
                $this->resource->withResponse($request, $response);
        });
    }
}