<?php

namespace Peer\Base;

use Illuminate\Http\Resources\Json\Resource as BaseResource;

class Resource extends BaseResource{
    public function toResponse($request){
        return (new ResourceResponse($this))->toResponse($request);
    }
}