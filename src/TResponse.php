<?php

namespace Peer\Base;


/**
 * 数据响应
 */
class TResponse
{

    /**
     * 是否为组合接口
     * @var boolean
     */
    private $isGroup=false;

    /**
     * 组合接口数据
     * @var array
     */
    private $groupData=array();

    /**
     * 接口返回状态
     * 1 成功
     * 2 失败
     * 3 登录失效
     * 4 没有权限
     * 5 非法请求（加密无效）
     * @var integer
     */
    private $status=1;

    /**
     * 具体错误信息（一般用于表单）
     * @var string
     */
    private $errors=array();

    /**
     * 简单信息（一般信息和错误信息）
     * @var string
     */
    private $msg='';

    /**
     * 具体数据
     * @var array
     */
    private $data=array();

    /**
     * 返回数据类型
     * @var string
     */
    private $type='json';

    private $headers=array();

    /**
     * 设置是否为组合接口
     * @param [type] $status [description]
     */
    public function setGroup($is_group){
        $this->isGroup=$is_group;
        return $this;
    }

    /**
     * 设置状态
     * @param [type] $status [description]
     */
    public function setStatus($status){
        $this->status=$status;
        return $this;
    }

    /**
     * 获取状态
     * @return [type] [description]
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * 成功
     * @return [type] [description]
     */
    public function success(){
        return $this->setStatus(1);
    }

    /**
     * 失败
     * @return [type] [description]
     */
    public function error(){
        return $this->setStatus(2);
    }

    /**
     * 登录失效
     * @return [type] [description]
     */
    public function loginInvalid(){
        return $this->setStatus(3);
    }

    /**
     * 没权限
     * @return [type] [description]
     */
    public function noPower(){
        return $this->setStatus(4);
    }

    /**
     * 非法请求
     * @return [type] [description]
     */
    public function noAllow(){
        return $this->setStatus(5);
    }

    /**
     * 更新
     * @return [type] [description]
     */
    public function update(){
        return $this->setStatus(6);
    }

    /**
     * 设置错误信息
     * @param string
     */
    public function setError($errors=array()){
        $this->errors=$errors;
        return $this;
    }

    /**
     * 设置信息
     * @param string
     */
    public function setMsg($msg=''){
        $this->msg=$msg;
        return $this;
    }

    /**
     * 设置数据
     * @param $data 数据
     */
    public function setData($data=array()){
        $this->data=$data;
        return $this;
    }

    /**
     * 获取数据
     */
    public function getData(){
        if($this->isGroup){
            return $this->groupData;
        }else{
            return $this->data;
        }
    }

    /**
     * 返回json类型数据
     */
    public function json(){
        $this->type='json';
        return $this;
    }

    /**
     * 返回base64 后的json类型数据
     */
    public function base64(){
        $this->type='base64';
        return $this;
    }

    /**
     * 获取数据返回类型
     * @return [type] [description]
     */
    public function getType(){
        return $this->type;
    }

    /**
     * 重置接口数据
     * @return [type] [description]
     */
    public function resetData(){
        $this->status=1;
        $this->errors=array();
        $this->errorMsg='';
        $this->data=array();
    }

    /**
     * 设置响应头
     * @param  array  $headers [description]
     * @return [type]          [description]
     */
    public function withResponse($headers=array()){
        $this->headers=$headers;
        return $this;
    }

    /**
     * 设置响应
     * @param [type] $data [description]
     */
    private function setResponse($data){
        $response=null;
        switch ($this->type) {
            case 'base64':
                $response = response(base64_encode(json_encode($data)),200)->header('Content-Type', 'text/plain');
                break;
            default:
                $response = response()->json($data, 200);
                break;
        }
        if(!empty($this->headers)){
            $response->withHeaders($this->headers);
        }
        return $response;
    }

    /**
     * 输出数据
     * @return [type]
     */
    public function response($key=''){
        $data=array(
            'status'=>$this->status,
            'data'=>$this->data,
            'errors'=>$this->errors,
            'msg'=>$this->msg,
        );
        if($this->isGroup){
            if(empty($key)){
                $this->groupData[]=$data;
            }else{
                $this->groupData[$key]=$data;
            }
            return;
        }
        return $this->setResponse($data);
    }


    /**
     * 返回组合数据
     * @param  [type] $route_list [description]
     * @return [type]             [description]
     */
    public function responseGroup($route_list){
        $data=array(
            'status'=>$this->status,
            'data'=>$this->groupData,
            'errors'=>$this->errors,
            'msg'=>$this->msg,
        );
        return $this->setResponse($data);
    }
}