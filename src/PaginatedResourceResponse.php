<?php

namespace Peer\Base;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse as BasePaginatedResourceResponse;
use Peer\Base\Facades\TResponse;

class PaginatedResourceResponse extends BasePaginatedResourceResponse{
    public function toResponse($request)
    {
        $data=$this->wrap(
            $this->resource->resolve($request),
            array_merge_recursive(
                $this->paginationInformation($request),
                $this->resource->with($request),
                $this->resource->additional
            )
        );
        $data['meta']['pagesize']=$data['meta']['per_page'];
        $data['meta']=array_except($data['meta'],['per_page']);
        return tap(TResponse::success()->setData($data)->response(), function ($response) use ($request) {
                $response->original = $this->resource->resource->pluck('resource');

                $this->resource->withResponse($request, $response);
        });
    }
}