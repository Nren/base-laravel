<?php

namespace Peer\Base\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * 
 * @see App\Extend\ApiReturn;
 */
class TResponse extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(){
        return 'tresponse';
    }
}
